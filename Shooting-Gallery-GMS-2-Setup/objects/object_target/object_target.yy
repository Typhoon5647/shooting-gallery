{
    "id": "31d03841-c614-42ce-b1b5-939fdff058a4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_target",
    "eventList": [
        {
            "id": "90907de4-bc9c-450f-875d-fc03c3dba0c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "31d03841-c614-42ce-b1b5-939fdff058a4"
        },
        {
            "id": "2ce61e2f-c820-4ce4-92e7-9fd457382473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "31d03841-c614-42ce-b1b5-939fdff058a4"
        },
        {
            "id": "30ff0e88-3706-4b22-8aba-54d29056d97f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "31d03841-c614-42ce-b1b5-939fdff058a4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1914408e-2d35-4bce-ad60-bcaa21fff27f",
    "visible": true
}