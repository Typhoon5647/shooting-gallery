{
    "id": "ecb78872-3f91-474e-9d00-dd52e8469844",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_target_bullets",
    "eventList": [
        {
            "id": "fc8014ad-a1bf-422b-b7a6-6271b93eda1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ecb78872-3f91-474e-9d00-dd52e8469844"
        },
        {
            "id": "a8f5b60f-3e1a-4f56-9655-2b293ddbdc16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "ecb78872-3f91-474e-9d00-dd52e8469844"
        },
        {
            "id": "57e03be7-891b-4651-a98a-e1d3f5b1fc52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "ecb78872-3f91-474e-9d00-dd52e8469844"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3541c235-f9fc-420e-82f3-c4d228de9721",
    "visible": true
}