/// @DnDAction : YoYo Games.Drawing.Set_Color
/// @DnDVersion : 1
/// @DnDHash : 3D09C193
/// @DnDArgument : "color" "$FFFFA70F"
draw_set_colour($FFFFA70F & $ffffff);
var l3D09C193_0=($FFFFA70F >> 24);
draw_set_alpha(l3D09C193_0 / $ff);

/// @DnDAction : YoYo Games.Drawing.Set_Font
/// @DnDVersion : 1
/// @DnDHash : 27ED0028
/// @DnDArgument : "font" "font_in_game"
/// @DnDSaveInfo : "font" "ccbfb7c2-2343-4217-9870-d931a3cc3636"
draw_set_font(font_in_game);

/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Score
/// @DnDVersion : 1
/// @DnDHash : 21E36E89
/// @DnDArgument : "x" "50"
/// @DnDArgument : "y" "10"
if(!variable_instance_exists(id, "__dnd_score")) __dnd_score = 0;
draw_text(50, 10, string("Score: ") + string(__dnd_score));

/// @DnDAction : YoYo Games.Drawing.Draw_Instance_Lives
/// @DnDVersion : 1
/// @DnDHash : 2212C06F
/// @DnDArgument : "x" "300"
/// @DnDArgument : "y" "25"
/// @DnDArgument : "sprite" "sprite_ammo"
/// @DnDSaveInfo : "sprite" "9395ce17-fde2-4130-8dfe-d93e356524aa"
var l2212C06F_0 = sprite_get_width(sprite_ammo);
var l2212C06F_1 = 0;
if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
for(var l2212C06F_2 = __dnd_lives; l2212C06F_2 > 0; --l2212C06F_2) {
	draw_sprite(sprite_ammo, 0, 300 + l2212C06F_1, 25);
	l2212C06F_1 += l2212C06F_0;
}