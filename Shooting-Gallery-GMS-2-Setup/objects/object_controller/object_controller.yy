{
    "id": "257099fe-d5a0-4cd5-b715-0e78714f48ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_controller",
    "eventList": [
        {
            "id": "5289fbba-4fc8-4f2d-87f6-993ec2a0fd0d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "257099fe-d5a0-4cd5-b715-0e78714f48ba"
        },
        {
            "id": "762bb095-50f6-4da5-89db-180aa4a02cde",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "257099fe-d5a0-4cd5-b715-0e78714f48ba"
        },
        {
            "id": "f4dc8062-af57-45f8-a2c0-35d573b7ba0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "257099fe-d5a0-4cd5-b715-0e78714f48ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}