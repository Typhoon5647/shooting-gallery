/// @DnDAction : YoYo Games.Instance Variables.If_Lives
/// @DnDVersion : 1
/// @DnDHash : 6DBA8338
/// @DnDArgument : "op" "3"
if(!variable_instance_exists(id, "__dnd_lives")) __dnd_lives = 0;
if(__dnd_lives <= 0)
{
	/// @DnDAction : YoYo Games.Common.Set_Global
	/// @DnDVersion : 1
	/// @DnDHash : 0A6512F8
	/// @DnDParent : 6DBA8338
	/// @DnDArgument : "value" "__dnd_score"
	/// @DnDArgument : "var" "end_score"
	global.end_score = __dnd_score;

	/// @DnDAction : YoYo Games.Rooms.Next_Room
	/// @DnDVersion : 1
	/// @DnDHash : 7E994045
	/// @DnDParent : 6DBA8338
	room_goto_next();
}