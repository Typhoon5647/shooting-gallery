/// @DnDAction : YoYo Games.Timelines.Set_Timeline
/// @DnDVersion : 1
/// @DnDHash : 15C4EB97
/// @DnDArgument : "timeline" "timeline_room_main"
/// @DnDArgument : "loop" "1"
/// @DnDSaveInfo : "timeline" "00d0c574-a0e6-4c22-8089-0aac0b96a19f"
timeline_index = timeline_room_main;
timeline_loop = 1;
timeline_running = 1;

/// @DnDAction : YoYo Games.Instance Variables.Set_Score
/// @DnDVersion : 1
/// @DnDHash : 1AC7D0ED

__dnd_score = real(0);

/// @DnDAction : YoYo Games.Instance Variables.Set_Lives
/// @DnDVersion : 1
/// @DnDHash : 0046B1AE
/// @DnDArgument : "lives" "8"

__dnd_lives = real(8);