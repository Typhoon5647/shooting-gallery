{
    "id": "ce11e51c-2b5a-4e85-ba8d-be73776452e6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_crosshair",
    "eventList": [
        {
            "id": "7b6e89bc-11c1-455d-8123-4646f3e5a22f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ce11e51c-2b5a-4e85-ba8d-be73776452e6"
        },
        {
            "id": "5066f926-bd00-48e2-afb3-7e10958e283b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "ce11e51c-2b5a-4e85-ba8d-be73776452e6"
        },
        {
            "id": "c67e2fd6-22c4-4bbb-8189-3b6ca7931870",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "ce11e51c-2b5a-4e85-ba8d-be73776452e6"
        },
        {
            "id": "9eb9408d-b95e-4351-9866-0587526e2c6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ce11e51c-2b5a-4e85-ba8d-be73776452e6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6b13dcd2-2334-4593-bac5-12446eff3b12",
    "visible": true
}