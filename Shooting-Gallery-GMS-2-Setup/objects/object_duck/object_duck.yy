{
    "id": "9ccd4c4d-b95c-4289-9d1f-de0af5934671",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_duck",
    "eventList": [
        {
            "id": "2bbd4a2d-f68b-433a-9e6f-7f7f8dced05f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9ccd4c4d-b95c-4289-9d1f-de0af5934671"
        },
        {
            "id": "c515cf78-3d4e-414e-bb0e-72b625ff7274",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9ccd4c4d-b95c-4289-9d1f-de0af5934671"
        },
        {
            "id": "fbafc005-aa7d-4090-8402-e909568aa903",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "9ccd4c4d-b95c-4289-9d1f-de0af5934671"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f6cc6b0a-b98f-4145-ad5d-4b07d5bf5886",
    "visible": true
}