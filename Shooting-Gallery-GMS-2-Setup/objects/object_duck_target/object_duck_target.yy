{
    "id": "00913680-f07c-477b-83d7-f1b9a32fb4f2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_duck_target",
    "eventList": [
        {
            "id": "b0d7d816-a3d3-4974-8215-b9fe1fea2dd0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "00913680-f07c-477b-83d7-f1b9a32fb4f2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "9ccd4c4d-b95c-4289-9d1f-de0af5934671",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a91c24d4-8c58-4636-8c9d-c15f55c4760a",
    "visible": true
}