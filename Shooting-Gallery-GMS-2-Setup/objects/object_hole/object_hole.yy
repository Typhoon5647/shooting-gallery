{
    "id": "920df11b-1f1b-43c9-a40c-d082e60f87ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_hole",
    "eventList": [
        {
            "id": "0fffc58d-01c8-4713-8e7e-b1a0a80f2974",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "920df11b-1f1b-43c9-a40c-d082e60f87ab"
        },
        {
            "id": "3f80c1de-e199-4e4e-b63b-6c3dfb2d7050",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "920df11b-1f1b-43c9-a40c-d082e60f87ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c688a3da-796b-4b78-b8c5-e47601a17548",
    "visible": true
}