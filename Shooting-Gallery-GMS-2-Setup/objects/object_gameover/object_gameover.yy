{
    "id": "49d04618-d803-4923-9921-2c79079dfcba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_gameover",
    "eventList": [
        {
            "id": "7aa2fd52-1d98-481c-818b-a514a2db22ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "49d04618-d803-4923-9921-2c79079dfcba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3b4c8159-e119-4512-b1a9-3b3a74dbf14f",
    "visible": true
}