{
    "id": "ccbfb7c2-2343-4217-9870-d931a3cc3636",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "font_in_game",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Gadugi",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "38681787-a9f1-4e47-bfa6-07d86cdf429d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 41,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "9f95c413-cf98-4bcd-882f-ff64ee7c298c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 41,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 172,
                "y": 88
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "96aad81f-1a34-4032-a173-5729dfb6a655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 41,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 159,
                "y": 88
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "543b8ee8-3c57-4042-a6ab-8dbc495b8387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 41,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 139,
                "y": 88
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "df073deb-b166-4b65-b1dc-dba468a80eff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 121,
                "y": 88
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "9f10ee9b-d8a4-4262-a8c0-28ed6b78c70f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 41,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 94,
                "y": 88
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f1252ff2-54af-46a2-9408-b6cf60f405b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 41,
                "offset": 1,
                "shift": 26,
                "w": 25,
                "x": 67,
                "y": 88
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "91e28c85-af9f-48b6-b577-e0747b168290",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 41,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 60,
                "y": 88
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b06e58db-d950-4c60-af8a-14a02d7c9d5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 41,
                "offset": 2,
                "shift": 11,
                "w": 10,
                "x": 48,
                "y": 88
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "4f168683-08a9-4691-b086-099740be44bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 41,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 36,
                "y": 88
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "76225b04-c1c3-42d2-98b5-a774b950f9ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 41,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 180,
                "y": 88
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "dfc0594c-0033-4262-aa3f-d954c344ac97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 41,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 18,
                "y": 88
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8b5eb5c0-fb58-4e29-8b44-c4ee3412a0c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 41,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 490,
                "y": 45
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6b0d46a3-f3cf-401b-83ac-7555726eb14a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 41,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 479,
                "y": 45
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "00a9bd59-b210-485e-b073-7bbf5dcfe8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 41,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 470,
                "y": 45
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "94b70085-7a7a-459c-b0a2-0a3c3b155465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 41,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 452,
                "y": 45
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b6427608-90af-419c-bf01-686d3f5e03c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 434,
                "y": 45
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c19bbf38-13d7-47e1-b285-4de92ef5e083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 41,
                "offset": 3,
                "shift": 18,
                "w": 10,
                "x": 422,
                "y": 45
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "8deba1c3-772b-42ce-ab3b-adbcb95b2ea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 405,
                "y": 45
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "29c16c2c-f84d-4fee-be72-2714c3bd98a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 388,
                "y": 45
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "a28daf53-6418-4302-b36b-aa6ddaefb74e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 41,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 368,
                "y": 45
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b6732f2c-c2ed-46d6-aa75-7f389bbfc9ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 41,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 2,
                "y": 88
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "a418341c-eb9f-4236-8234-360060965287",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 195,
                "y": 88
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "742e6c0d-eaaf-40db-9838-73257c1878d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 213,
                "y": 88
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "dbc0b215-044a-4895-81d6-7e8561c1a2da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 231,
                "y": 88
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "9dbeefa2-bcac-49a2-9195-50bb02eb7f51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 41,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 118,
                "y": 131
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "20cf2422-492d-4234-a167-130aebbfbb85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 41,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 109,
                "y": 131
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "a055759f-807c-4240-a5ee-2c2aea3b7777",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 41,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 99,
                "y": 131
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "cee1a503-d6c4-4a56-a215-3e1fc79a9577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 41,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 81,
                "y": 131
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "893f56d2-9a2a-413e-a3e5-cc665830126b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 41,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 63,
                "y": 131
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "a12a3cf7-7b03-449a-9e41-97129ed2cb25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 41,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 45,
                "y": 131
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "8bad95f7-becf-4fb0-854d-d3ce6c2b7872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 41,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 30,
                "y": 131
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "bde3f0b9-b485-4256-892c-a25ab8912872",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 41,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 2,
                "y": 131
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "8f19e821-4a0f-42ec-8632-e877df07a4e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 41,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 477,
                "y": 88
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "51be883e-742f-45ab-9eba-27068cc4a528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 41,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 458,
                "y": 88
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "be88f678-b134-49cd-8e06-1e607b0adb43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 41,
                "offset": 1,
                "shift": 19,
                "w": 18,
                "x": 438,
                "y": 88
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "e87cf48e-b601-46ad-a369-5bf962adceb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 41,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 416,
                "y": 88
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "4bbde4e2-5ce4-45e3-8174-02bfa1f89f10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 41,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 400,
                "y": 88
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3078e17d-6284-4fc1-92f8-b257255ca8e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 41,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 384,
                "y": 88
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "55e8d9bb-549d-4c09-9d4e-de149af2a653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 41,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 362,
                "y": 88
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2fc92fec-6bbc-4438-b4ba-d6e3c47a7d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 41,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 340,
                "y": 88
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "40777a51-93e3-4e4b-b6a3-4c661dcd4109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 41,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 328,
                "y": 88
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "14b4de6d-e711-4f34-9990-50ca8c3e169e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 41,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 314,
                "y": 88
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "48e9fe59-c2df-40e5-be4e-145644a93b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 41,
                "offset": 2,
                "shift": 20,
                "w": 19,
                "x": 293,
                "y": 88
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "afa6c130-0ccc-44ac-b2ea-29ea56aa41d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 41,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 277,
                "y": 88
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0109b12d-6486-40e4-b1a7-1aeb6edcb83b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 41,
                "offset": 2,
                "shift": 30,
                "w": 26,
                "x": 249,
                "y": 88
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "34952449-016e-4c40-8b2e-3d53adaef935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 41,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 345,
                "y": 45
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5bd8ccf3-6e92-4505-aa8c-b9350820e093",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 41,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 321,
                "y": 45
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "4b24056f-ee7b-419f-ada2-5e18e3cff470",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 41,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 302,
                "y": 45
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "a7301b90-7e4d-4c1b-a742-cfb8d579f5b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 41,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 398,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a141d016-f94c-43fd-9d3f-b4128303dcd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 41,
                "offset": 2,
                "shift": 20,
                "w": 19,
                "x": 367,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "c4ed19b2-8ed3-4809-9c79-64be526f2300",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 41,
                "offset": 1,
                "shift": 17,
                "w": 16,
                "x": 349,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "d4ace76f-2c65-41fb-960a-258950d1de4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 41,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "bb15ab89-5b38-462b-9136-769fdf22ff10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 41,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 308,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "aa9f7748-061d-4605-9940-8aa70c3c1ebf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 41,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "797d77d0-ebbf-4063-845e-59162e2ac163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 41,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 252,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "25589b1d-6b4a-4206-bc12-db217bc3ce47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 41,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "f324f641-acf7-4144-a88f-54ac304b6ec3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "42419a96-9bba-44d5-b765-312671afbfaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 41,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "3fbb8622-4e94-46f2-8bfc-41068564be83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 41,
                "offset": 3,
                "shift": 11,
                "w": 8,
                "x": 388,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "85d8e610-61cc-4d8b-abbe-ebad9c30934a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 41,
                "offset": 0,
                "shift": 14,
                "w": 15,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "9c00d5fe-3ac4-4918-ba7b-80f4dec5be8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 41,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 145,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0f6132d7-2369-447e-b716-832af4dfc26a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 41,
                "offset": 3,
                "shift": 22,
                "w": 16,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "7ca1c1ea-3b99-450b-8990-b560e5e03a72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 41,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "cf22211e-53b5-4cf2-b41d-0495de6af0e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 41,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "746cd01a-214d-4675-898d-297c396ab89c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 15,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d76d5f69-e686-4650-84c8-597e4d038416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 41,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "cbc536b6-980f-4a1f-8a15-243b9a8d9d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 41,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "04a6e330-c0c8-4fcc-a780-dd302388c102",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 41,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "e423d8e2-2a15-4faa-b7de-43f27b8921f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 41,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "b1d0ee47-5cb6-4658-a004-e57995a6a26b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 155,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "da451448-7f4b-4c1b-b228-48fb64d347a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 41,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 423,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "51899f5c-62ff-4b59-81d1-4868388a2fbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 41,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 105,
                "y": 45
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "4d1bc0e4-fa40-4ff5-90ba-30ae1dbfb4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 41,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 442,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "7042abce-6ab3-4c35-9b29-88dd518a0e0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 41,
                "offset": -3,
                "shift": 9,
                "w": 11,
                "x": 275,
                "y": 45
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "203df751-c9e9-45bd-a3a7-44584598736e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 41,
                "offset": 2,
                "shift": 17,
                "w": 16,
                "x": 257,
                "y": 45
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "1da3a7e4-b0e5-4002-8713-69dd6234aa8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 41,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 250,
                "y": 45
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d79b60ea-157b-4a84-894e-63f5322d0d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 41,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 223,
                "y": 45
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b791730f-1d8b-4b72-8c92-6a4285413e06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 41,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 206,
                "y": 45
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "d05f68bb-2a73-433b-b5c4-9a10756292f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 41,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 187,
                "y": 45
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "290d9056-b15f-4d2e-812a-7e8ae15d52ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 41,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 168,
                "y": 45
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f00cc1a9-73e4-4c84-bcd1-097b8b7f1e0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 41,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 149,
                "y": 45
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "b083536f-8ad0-4d6c-b3ff-8ae2d241d67f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 41,
                "offset": 2,
                "shift": 12,
                "w": 11,
                "x": 136,
                "y": 45
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4ebfb475-b1e4-4a24-a553-b73d0dd845bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 41,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 288,
                "y": 45
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a7cd45bb-4a49-49c3-a372-ea3b6a89ff60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 41,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 122,
                "y": 45
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "2d116561-5e02-42d0-b3ec-6b9b040a9084",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 41,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 87,
                "y": 45
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "8541bea8-9d8f-4db7-a70c-508978f4c5b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 68,
                "y": 45
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "78bb8840-74c6-43b4-9ba9-b1f9b5dbf493",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 41,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 41,
                "y": 45
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "cd4f8453-fec0-4377-971d-5895a5a2b7f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 41,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 22,
                "y": 45
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c9345e0a-70d0-471d-b084-ba1347ed88b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 41,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 2,
                "y": 45
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1cceaa61-f212-424f-a9bb-533f00b9bd8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 41,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 480,
                "y": 2
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "86737b03-da9f-405a-861c-614ac1061481",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 41,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 468,
                "y": 2
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3648b588-3a64-4e08-85be-29cf4f9e66b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 41,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 462,
                "y": 2
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a008baf7-275e-436e-9c47-9c1003805194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 41,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 451,
                "y": 2
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "faaa5a23-6094-4f50-b5ff-94ffec7059bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 41,
                "offset": 2,
                "shift": 22,
                "w": 17,
                "x": 136,
                "y": 131
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "c1d4eddb-4557-4cfc-ac65-cfd2c3d78db0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 41,
                "offset": 6,
                "shift": 30,
                "w": 18,
                "x": 155,
                "y": 131
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "71b6d618-8532-4b9c-ba47-c5fb6c5f5534",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 114
        },
        {
            "id": "16b6913d-5bfe-4d65-9e88-a07585327a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 115
        },
        {
            "id": "593fe4bc-6779-4f40-a5a7-f90acdf12b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 34,
            "second": 353
        },
        {
            "id": "b45a994e-5c8c-45db-a2b8-1e2aa63aa276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 114
        },
        {
            "id": "ae103157-d753-41e5-bdef-9d78f5a46227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 115
        },
        {
            "id": "289e3594-34c0-435d-a4d1-16f4ee8749fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 353
        },
        {
            "id": "b254f093-704a-47bf-b4c8-ea9ba1847478",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 40,
            "second": 106
        },
        {
            "id": "62b68191-4495-4b94-a849-6b2881b0d190",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 65
        },
        {
            "id": "919d7267-37e9-412f-8976-39897b81a242",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 74
        },
        {
            "id": "b2d6ae3f-75a8-4bb5-a1c8-61b50a75d92c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 99
        },
        {
            "id": "310d26cd-a0d6-46ed-8fa7-31ddd838cb9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 100
        },
        {
            "id": "5a58a056-d847-4a55-848b-43ad20e5964e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 101
        },
        {
            "id": "96ee38a2-910a-483a-b980-ff1ffc6bd6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 103
        },
        {
            "id": "f90876db-aaee-4f12-b765-6942e891c3ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 111
        },
        {
            "id": "62d92d12-6c08-4baf-8a20-ed3cb9dc3ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 113
        },
        {
            "id": "92b4776e-9530-4e1a-a1aa-ef0eafdd55e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8216
        },
        {
            "id": "792ad61e-d6f1-4389-876e-537666d4c20c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8217
        },
        {
            "id": "045cac08-5d17-4fbc-80e1-ad40b953159f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8220
        },
        {
            "id": "9a12f7f0-8cc6-4506-9aba-403ceb00ddbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 44,
            "second": 8221
        },
        {
            "id": "21091dec-a682-4116-b11e-1a4dbd5ecad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8216
        },
        {
            "id": "645a1e4d-b89e-4385-bfa6-cb51dcb3aa57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8217
        },
        {
            "id": "feeeae40-d4df-4cf1-bedd-c6ca25b3e071",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8220
        },
        {
            "id": "c8843b96-039a-481a-882f-fb4de5144789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 46,
            "second": 8221
        },
        {
            "id": "b7710571-6259-422f-b178-f84de02b694a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 42
        },
        {
            "id": "016fb177-ad9d-4f04-be03-921d72a4f6b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 44
        },
        {
            "id": "3c7f14f5-682f-46be-9773-02d32b8188bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 59
        },
        {
            "id": "f146a57d-9e4f-4206-8d14-1a4fc4ae6095",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 74
        },
        {
            "id": "514fea15-6e8b-49de-94d7-f5df8de2f3f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "d5b2d6ce-4565-45f7-8aa7-0fda4b246b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "8d0975df-b7c2-4e4f-976f-ca5dc9d62fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "31aaebc0-361b-4add-ba75-e8ceba8b6913",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "bfef0f02-e77d-4f5f-9595-2b7df44f3c8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "d155a7e5-db5d-4356-a138-77259f4f62c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "a05fbc8e-c7cc-4948-a786-495e33f82f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "6004c6da-e76c-4143-890a-de895d23ed16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "79ca34e6-916b-44b4-a074-baf6f69bb7a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 221
        },
        {
            "id": "faf44780-bbe2-4a64-9d1a-dddb8e48e617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "4384cea8-492c-4a69-b953-6a5de95557d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 376
        },
        {
            "id": "74456e87-41de-44fe-b569-679ba6b43714",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 65,
            "second": 381
        },
        {
            "id": "86e9a7b1-3f74-479e-be5c-53f5504c075a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8216
        },
        {
            "id": "abffdda4-d2a2-49e4-b1b3-5ba801540f00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "a110e57e-855e-4388-ae64-462f867de045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8220
        },
        {
            "id": "e9ac9509-92cb-4132-8f75-235a59420198",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "5b659147-6546-465b-853b-b809dda5eaea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8482
        },
        {
            "id": "a76281e5-b8c0-436f-94c2-c102a2c4413a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 84
        },
        {
            "id": "6d137661-1ccd-4d88-9ebe-01aec8c764b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 89
        },
        {
            "id": "8c79ef09-8da4-49ba-99de-39a6113799e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 221
        },
        {
            "id": "d7cf4796-eb6a-43c9-b31f-8ebea6f8d7d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 376
        },
        {
            "id": "5ea9ef68-727f-4cb3-87ac-f5b049a277e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8218
        },
        {
            "id": "fad6b47c-104b-4c84-b756-30d3f695fe46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 66,
            "second": 8222
        },
        {
            "id": "669b6bac-e017-48ff-a878-be90f1b183b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 67
        },
        {
            "id": "6e5f34f6-a163-436d-939a-aa0dbf652f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 71
        },
        {
            "id": "1b889e26-b9c6-4988-955e-c881c9368d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 81
        },
        {
            "id": "0d9b78ed-f517-47a3-aae3-2cbb2946f51d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 171
        },
        {
            "id": "d783d60c-713a-4281-908c-ab78b5d7f23e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 199
        },
        {
            "id": "dc264d24-a94e-4e33-8026-965cd300548c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 210
        },
        {
            "id": "bff978cf-72ac-4e84-ad3d-9affa595064f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 211
        },
        {
            "id": "5407c8e1-737a-45fe-9101-856d6b8c140f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 212
        },
        {
            "id": "3d756b56-8f24-4f88-a5e0-6bbca4bae308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 213
        },
        {
            "id": "fc504516-dc4f-4916-b61c-3f41f1abe921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 214
        },
        {
            "id": "58b9a7d9-cc80-4853-8769-2b8adf49d121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 216
        },
        {
            "id": "9a0eb395-eed2-47ee-8102-2cd94567bab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 67,
            "second": 239
        },
        {
            "id": "eab443a5-9f94-4d72-bcab-1a8f9290d912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 338
        },
        {
            "id": "1feb44fa-3afe-4c13-8e81-998976159231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 8249
        },
        {
            "id": "0c5ffad7-39c6-4e07-9304-1f6ed6afbdfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 44
        },
        {
            "id": "46cd0393-7fd2-4d44-8768-22f1ebb5b041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 46
        },
        {
            "id": "aa756de9-7e1a-4e02-9a10-e5fb692569aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 84
        },
        {
            "id": "d7d8ff3a-ec61-47c3-8ed1-258b9d21c774",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 88
        },
        {
            "id": "781352f2-15e9-44b6-8660-8d8d5880dfcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 90
        },
        {
            "id": "74161a17-0e34-40a9-aba2-7281b7962282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 198
        },
        {
            "id": "ad130bf1-1eeb-46a9-890c-ae0b37113d24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 68,
            "second": 381
        },
        {
            "id": "5eb7f72a-afa7-4425-aa38-b7b1ac3d677f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8218
        },
        {
            "id": "29db0d53-d0c4-4ea8-a916-fb016abb4b59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8222
        },
        {
            "id": "edeab02c-f815-4d33-9215-653bd0cd54b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 68,
            "second": 8230
        },
        {
            "id": "373e1d6c-4c81-44a9-b2c5-6cc5faa60a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 74
        },
        {
            "id": "e2c9f04e-8e39-4129-abeb-6ac17f3ee7c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 87
        },
        {
            "id": "29e18912-e51b-4ee4-8aed-fe11d8c34a18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 88
        },
        {
            "id": "a2381522-0c5b-40f4-b848-5fe773444503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "198107ff-ab17-42c4-813d-cff69bce6231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "9887ebcb-d00e-4847-8ea1-bdc883eb0cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "b75ed809-a371-46f3-aee9-b65ede21a4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "c387ff4e-8d33-425d-920f-ec0341914cac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 97
        },
        {
            "id": "ff1b0178-f370-45f2-b926-c926ac6518f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 192
        },
        {
            "id": "cc7481f6-0c74-47cb-a0b1-6870317b7977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 193
        },
        {
            "id": "deeda16c-441c-45fe-ae6e-53eceb730367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 194
        },
        {
            "id": "b13c0a56-8d58-4371-96a8-d4091f937bb3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 195
        },
        {
            "id": "3dc4dd0e-8dc5-45a8-8677-f8e3f84a2006",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 196
        },
        {
            "id": "ea1815ab-68ab-4946-92e2-77be4c9b87bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 197
        },
        {
            "id": "fa4fc379-8e4d-4f39-866c-78430f0b44df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 198
        },
        {
            "id": "1feae879-ca10-4aba-9623-09d27f27b72b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "b9c8c052-40fd-40dc-8c12-8aa58a5cdcd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8218
        },
        {
            "id": "4b3283b0-de3a-4d8b-8903-4f19daefd140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8222
        },
        {
            "id": "9729fe2c-d7a2-45c8-800e-f375b0c56ce2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 8230
        },
        {
            "id": "81a1cfc0-d4b2-4c6c-8005-0558f65bcfb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "d70b4afe-1bdc-4c1f-9638-51cf48ba4ddf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "3f151e96-956e-4263-922f-03ba53cf14a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "ddd1ff19-ad8f-47a9-8ec9-4811cdafe6fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "d2e53b55-f4e4-45a3-9bab-4e5580f72c48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 74
        },
        {
            "id": "0c43a597-0f75-46b0-b2b2-e918c390c80b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 192
        },
        {
            "id": "af0601a8-1eae-4931-8988-582a5d950575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 193
        },
        {
            "id": "36aa8067-ae06-4776-bb7c-c56259925bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 194
        },
        {
            "id": "546bfaa8-34e4-4c93-b526-bdf93ea9dc52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 195
        },
        {
            "id": "4c544db5-5f7a-4796-b604-0cc9d119f279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 196
        },
        {
            "id": "ab94791f-41c9-41b6-9378-c64b8812c992",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 197
        },
        {
            "id": "16bc2e22-0d76-469a-97a7-0610b5d1b33b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 198
        },
        {
            "id": "a9e9dce7-13dd-4dae-8e88-6b101f72e334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 74,
            "second": 8218
        },
        {
            "id": "f02ae0db-66b6-487b-aad3-cf001fa2d9c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8222
        },
        {
            "id": "5ee47dd5-9966-41f6-bdc0-c5df670e3cc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 8230
        },
        {
            "id": "d12655b4-52c8-4834-af85-8c1f03ad6ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "ee64baa0-6c0b-4846-a0b2-6d3fd00f17f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 59
        },
        {
            "id": "38d95a12-6969-4bc6-add8-06f9c159cbaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "e80c4414-4669-4471-b45b-7003dcb22a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "71efc564-768f-42e8-895e-fc5c1599132b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 74
        },
        {
            "id": "7465556f-8331-4a33-8074-0ce0669fe33f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "4037e372-ad99-4b0f-9546-c80f93816856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "f0c3336a-36ef-4af3-8841-51b3ac281b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 88
        },
        {
            "id": "2a284bf2-59d1-410c-aad2-6286be168c6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 90
        },
        {
            "id": "30dfe2f5-a639-48f0-a7c3-c9bc7b7d35bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 116
        },
        {
            "id": "6b9c6910-263b-4b83-be4e-4965bfa23f06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "68350c18-9371-4515-8b56-f71499115202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "5cff77bf-053f-41e4-9598-262388c8d9f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "ba769997-b914-4ed8-832b-6c96474dfe7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 199
        },
        {
            "id": "b8fd7981-36d3-434d-9f96-dc22df6e259f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 210
        },
        {
            "id": "224de26e-0aa6-48ea-b4ab-cf99295acdb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 211
        },
        {
            "id": "4cb8a8b1-7e77-4f04-b480-74e844203e5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 212
        },
        {
            "id": "9f57a201-3c12-479e-bf22-2b554163a373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 213
        },
        {
            "id": "7cfdc4e9-d4ff-4dcf-9aa9-c39dd0a4ada2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 214
        },
        {
            "id": "8b904858-21a5-4c86-b584-b404afd592ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "de40a431-75f4-4898-9dd2-d19c192d6d1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 236
        },
        {
            "id": "f527aaa3-4cd5-4da6-a221-4a34f91abc38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 237
        },
        {
            "id": "a7179811-e3b7-4ed9-a695-7acc6c63834b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 239
        },
        {
            "id": "8bc29e0d-f0c8-4aa3-b80c-2fecc065f1c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 253
        },
        {
            "id": "422c3622-034d-40d3-8e39-95c42d84d2ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 255
        },
        {
            "id": "765c4490-8ac8-4f66-a79a-3f30f35840f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "dca34a46-f200-475f-b1d2-9bac481f3882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8218
        },
        {
            "id": "ba51a346-ac4e-49ad-90e4-2b259543ae52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 8222
        },
        {
            "id": "0b925441-f8b2-4db3-a95a-c358c8f75ff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 42
        },
        {
            "id": "ff207a20-021f-42a5-a0de-7ad9cba2cacf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 63
        },
        {
            "id": "a2151608-d47c-4435-bdd8-ed57637de676",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 65
        },
        {
            "id": "f6b4e690-e327-46b0-b3ae-1e3d196adb3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "86a4ee5e-a30f-47f0-ace9-92a25e24df43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "f9357ca4-03da-419c-bad2-d286dca42c94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 74
        },
        {
            "id": "0232baa7-1f55-48b0-8a42-02ac9b69cefc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "1b8d3510-be22-457d-a31e-9bc1b95d1617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "d0b300a3-f778-4cc8-9ad5-25cb7ab0f3a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "a058b484-f226-4939-a1ba-3c95585c252d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "4f26f9c2-51cd-447c-aeb5-2ce1d7bed258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "c8f93501-0036-495b-85ad-b59c7a5d63f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "e38e2711-1898-4da1-9cbe-f0d321de55e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "ba345494-bdfd-411a-b19e-a927e01a1954",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 90
        },
        {
            "id": "ddf248b8-03d7-4190-94eb-8836df6981b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "f761835a-f847-4f99-9adc-06baa13a6d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "867bcd2b-aff5-41ca-833d-835df5b36b88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "9a2d77c3-dd49-499c-8ae0-d86791f56aa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 199
        },
        {
            "id": "bde25fc3-52ab-403f-a07e-ceef7d3a45bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 210
        },
        {
            "id": "8d94a705-597c-4824-97df-481ec55ed281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 211
        },
        {
            "id": "da246188-f94a-45f2-b9fe-ae0bdf3fdae3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 212
        },
        {
            "id": "eb9643e1-e330-4e13-ae66-6e854de1f8bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 213
        },
        {
            "id": "52f3dfe3-672d-4a4e-b580-ec242e51102c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 214
        },
        {
            "id": "55c6030a-9d43-42cf-ba51-cf82d8d3005d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "4e70d664-0993-48b9-ba3f-ec4505787424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 217
        },
        {
            "id": "f09db126-09b4-4d3b-b6a7-c9cc37bf9eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 218
        },
        {
            "id": "ecf41e45-c6d6-4f87-b8ef-0c59ece48bfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 219
        },
        {
            "id": "f0eb2b8a-876b-4260-8a55-5e3a7e3ed86b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 220
        },
        {
            "id": "63cb8d5a-2c1e-44bb-b497-487a3f41f252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 221
        },
        {
            "id": "f6a473d9-039f-41e3-9e72-04888f094ad1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "3471e9a3-79c1-47be-b4c2-f98886d376f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "6adc45e7-0f67-419e-9b05-bdfa8b7e53f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "70d287ce-174c-4b27-93a0-b8ab4c679f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 376
        },
        {
            "id": "2a707720-347d-498e-99d2-7d0fd8b791ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 381
        },
        {
            "id": "53aaa691-ecd4-4104-822d-dfd4856aaafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8216
        },
        {
            "id": "e3f8bf00-2584-48cf-9194-d46d4a3c3bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "b12088cf-f878-4ba2-9e32-d670e1ace800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 8218
        },
        {
            "id": "e20da49e-1275-40ba-a601-c0bb167a67d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8220
        },
        {
            "id": "a1b3e778-a32e-4a17-b23c-c44eac827aa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8221
        },
        {
            "id": "ae889cd0-08b0-42ef-8afe-29981af691f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 8222
        },
        {
            "id": "093a4ba6-9ec2-4ee0-bfd5-74810225081d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8482
        },
        {
            "id": "d5cb000b-8151-414a-bfd5-df808b40edf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 44
        },
        {
            "id": "2bbaf5d6-9610-4505-b2a0-59e3216c48ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 46
        },
        {
            "id": "c263e866-488b-4c4c-afe6-ea3fca57def0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 84
        },
        {
            "id": "420bb722-37a3-4b80-a478-401ee5c9c38d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 88
        },
        {
            "id": "ffad8c56-6662-43f6-a818-f8c1229d0490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 89
        },
        {
            "id": "c81b8d4f-5aff-4c81-92e0-f8a363b6ba19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 90
        },
        {
            "id": "a9275f37-1936-439a-af84-a77a15e0ec6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 381
        },
        {
            "id": "0466c2ae-da69-4455-9148-0e474c05fb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 79,
            "second": 8218
        },
        {
            "id": "36855fc9-629d-439c-a95f-b217e4cd43ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 79,
            "second": 8222
        },
        {
            "id": "d57c4d42-bf6c-497b-b4d7-75063c76284d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 79,
            "second": 8230
        },
        {
            "id": "d03b0131-0c14-4b7d-a16e-7971b2dd4e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "abef9e0c-4217-4aa2-abed-d8f63adb2b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "603f0522-b73f-4c88-be01-1770ef02de8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "430daee2-fa95-4261-8dcc-366d76d85ed7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 74
        },
        {
            "id": "4d7f14d1-dcb9-4fec-8a9f-09eb7cfcdfaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 80,
            "second": 87
        },
        {
            "id": "7dc79b2c-04fc-47b3-b934-2579df40eca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 88
        },
        {
            "id": "d682d951-fb69-4e99-afd2-0dccbcb09feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 97
        },
        {
            "id": "c527abcf-eb72-44f9-8685-45d5cad4c76e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 99
        },
        {
            "id": "237e3ef2-e44e-4d2b-8c55-dca2230f07d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 100
        },
        {
            "id": "c740f388-7cdf-4ccf-bb23-fc047dff332a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 101
        },
        {
            "id": "9eb3b814-7642-4c41-b460-363c09cd7e1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 103
        },
        {
            "id": "be1b845f-c0e6-4e42-ae0b-4299853d29d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 111
        },
        {
            "id": "be40794f-65eb-40c8-a65c-6ce0507354de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 113
        },
        {
            "id": "0ed3229f-dd34-4219-a1a0-7dfb81228a9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 192
        },
        {
            "id": "057ae022-67c4-4787-a552-e7b36897b274",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 193
        },
        {
            "id": "28052328-ea38-4a49-b0e9-4b6e0cbfe96a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 194
        },
        {
            "id": "c7c5af16-6067-4877-8c86-66d560b1143e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 195
        },
        {
            "id": "167fee77-8e3f-49ba-a807-b220b53dfc20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 196
        },
        {
            "id": "c69b5691-f6a5-4d21-822b-9fc4005a5609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 197
        },
        {
            "id": "2a79f90f-e4d1-4e35-a9df-238ea8ec9102",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 198
        },
        {
            "id": "2e24ba50-b9fd-4555-b7b5-bc3f3c06fdf7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 224
        },
        {
            "id": "44e9e8f0-b407-4e9b-9578-7a14a268e4e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 225
        },
        {
            "id": "75fea45f-11bb-4fae-8f27-e813b0934d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 226
        },
        {
            "id": "cb1d0232-9424-4aa1-80b2-17611b0ac92a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 227
        },
        {
            "id": "b51278e1-be98-466c-b8c1-133656ab9092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 228
        },
        {
            "id": "24521981-f939-4ce8-843e-90cdb85030c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 229
        },
        {
            "id": "7bdcfcce-75e4-4c6e-836f-d8b49c3b84e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 230
        },
        {
            "id": "81d47c4f-3865-4c67-8179-94baea2a6d1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 231
        },
        {
            "id": "b636d089-ad2d-446a-bb70-de5f75f74db3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 232
        },
        {
            "id": "7d86b9bc-2ede-4e02-b5ce-1e2c412d3007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 233
        },
        {
            "id": "2af4e9c7-280d-4cc1-8d64-59a5abb82147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 234
        },
        {
            "id": "1366c419-a3ed-4c1e-b5b7-e7029b7820e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 235
        },
        {
            "id": "d593cf43-7c7e-42da-bd87-2b11bcc29e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 242
        },
        {
            "id": "0cb3fa5d-b088-475d-a379-c70bb41574ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 243
        },
        {
            "id": "e631b152-dfec-4177-84ad-e2f56eada37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 244
        },
        {
            "id": "805eb219-d9f5-46ed-ae90-1dc8ca284b63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 245
        },
        {
            "id": "2c09fbf5-d1fb-4663-95ba-50735ddb3fed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 246
        },
        {
            "id": "c3d0c0b8-c6f2-49e5-b5be-ffb65010818e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 339
        },
        {
            "id": "b6f87d5e-7278-44b4-8dd4-54d130d961ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8218
        },
        {
            "id": "1911941c-f748-4ad2-968e-146756740c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 8222
        },
        {
            "id": "e4a08436-09bc-4c66-9577-a19920b03f14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 8230
        },
        {
            "id": "47a4af7f-3662-47c8-a76c-ee54f31d6a5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 44
        },
        {
            "id": "54b1f1aa-8e55-4c82-a3a1-0dda65b41515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 46
        },
        {
            "id": "498a2732-99d7-4045-a334-b85f53029d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 84
        },
        {
            "id": "295841dc-3b28-4635-97fa-9a0e69c10eef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 88
        },
        {
            "id": "0b7ca51f-b5a8-4fa4-922d-c7bb5b45a72d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 90
        },
        {
            "id": "2f055c64-46fb-4d80-af23-8310502e8948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 81,
            "second": 381
        },
        {
            "id": "bc4e543e-0056-4da8-963a-edc9947e7116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8218
        },
        {
            "id": "223473d2-2a6b-48ff-93bf-6bcd7fbeefc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8222
        },
        {
            "id": "4cbc4d0f-fa72-4a9c-9e8b-f9c11094f01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 81,
            "second": 8230
        },
        {
            "id": "2aa83b6e-1ae8-409f-9420-556a6f88ef3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 59
        },
        {
            "id": "94ac9969-289c-449f-82ac-d63a8cbf7b36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 74
        },
        {
            "id": "af29f646-8dee-4f6f-9a3e-896d82edd9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "1193ab39-97d8-4ae3-ad7f-1ff30cddc0ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 99
        },
        {
            "id": "736fa55c-feee-4787-ba2b-92f790d0c373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 100
        },
        {
            "id": "8943574e-eaaa-48bd-b5eb-94c02a91830d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 101
        },
        {
            "id": "989c5733-2ba6-4624-9f21-9500de7594d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 103
        },
        {
            "id": "4d3b2327-b887-4bf3-85d6-bfc29f41c95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 111
        },
        {
            "id": "4bc981ee-7816-412b-92c8-123922fc10cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 113
        },
        {
            "id": "307099db-28ae-4842-83af-e863de01e863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 231
        },
        {
            "id": "e6d2459b-cbf6-41ab-ad31-74451589367a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 232
        },
        {
            "id": "1e604629-4a75-446b-8939-908bff145c37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 233
        },
        {
            "id": "b8804173-1e48-4a9c-b657-7f02746268ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 234
        },
        {
            "id": "69be6764-250e-4367-8297-d58f407a6aa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 235
        },
        {
            "id": "f322d388-efc2-435c-84ea-eb1ad118d30d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 242
        },
        {
            "id": "2801d3c2-b4ce-446c-adae-b6511276727e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 243
        },
        {
            "id": "34411a01-ea2a-46f9-b89b-b58571b8cc80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 244
        },
        {
            "id": "e7b02be2-3fc1-4a32-9132-bf0b00ab1f9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 245
        },
        {
            "id": "853d2446-679d-438f-aa54-6db5727a932e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 246
        },
        {
            "id": "c0c5992f-f559-4c06-a9bb-9f8db844fc83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 339
        },
        {
            "id": "cf356ebf-c67d-4d91-948a-cef20f6569de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 116
        },
        {
            "id": "63268c9b-8e47-47cd-b503-7c249b16ee85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 118
        },
        {
            "id": "f07a00be-e5cf-4b26-beba-9385ecdbe243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 121
        },
        {
            "id": "90e7be69-9ffa-4568-9500-13846f034b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 253
        },
        {
            "id": "b483a0d6-de9b-499b-9907-6bc6d5e5217c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 255
        },
        {
            "id": "85319309-8c27-498e-b24d-34a9b25dccfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8218
        },
        {
            "id": "d7aa240b-f1bb-479d-ac0c-68b1dcff886e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 8222
        },
        {
            "id": "07164748-78d3-45e9-8b9d-df0f4cca5116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "a5bc4f59-c0e8-48b7-8965-f5625f7a620d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "788a4efc-9d44-49d4-831a-d0769bc87b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "6aa66cc5-8c85-455d-9526-abce98da8f1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 67
        },
        {
            "id": "d6b21e09-68d8-4b7f-b3cf-f7acd50aef66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 71
        },
        {
            "id": "a2f7ec36-e745-4ad1-b750-85f7e09d7b85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "4784581d-a958-4220-afab-63e497358243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "4605a31f-3752-44e8-b631-3118b04e6d82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 81
        },
        {
            "id": "b3038ed2-484b-4040-9968-7ac1d7abf8cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 84
        },
        {
            "id": "23f1143e-eb17-44be-b131-b8821ca58331",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 86
        },
        {
            "id": "bc38d019-8aed-4c15-a2bf-1d18cd762b60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 87
        },
        {
            "id": "f1f78efc-2123-419c-8558-b79c2af92d0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 89
        },
        {
            "id": "c2202650-e681-4822-8d45-981e240d299d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "62e610d1-84da-4092-86b2-720992d68ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "e0d1223c-8db1-490c-ad02-d25e373553c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 100
        },
        {
            "id": "277ae428-871f-4027-8584-05f4dbaef46b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "1689bb18-3648-4d78-8969-7201aec8798d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 102
        },
        {
            "id": "33009adb-6bff-41b3-a4ba-cfe41ec40509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 103
        },
        {
            "id": "14013244-5a7e-4514-83c0-6eea3a382eda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 109
        },
        {
            "id": "539d24a9-c2f0-4621-a795-c48b700a9d0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 110
        },
        {
            "id": "be0cae9c-59a9-46f1-bb8a-f0fc06e444f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "a21262d9-84fb-48e2-8343-4ccf67d15adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 112
        },
        {
            "id": "65ea0860-4017-486b-983f-7dff8a74861b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 113
        },
        {
            "id": "1e7b5fa5-1a52-4d3b-bf8b-f447758dac2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "d0b11da4-3deb-4f2e-a8c6-a12ada254df3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "4bce09a5-7fd4-47d9-a8af-efa59fb46f11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "a0d6f8e7-8c30-42e4-a4ff-0155ecbc2963",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "43362f9b-af7c-44a1-aa3c-2e1d4acc7db1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "eb498f39-96ac-4e2e-9c56-581c6180e529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 120
        },
        {
            "id": "b54a8f6f-059b-43ff-93bb-43172f76be5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "bee91906-125b-4519-b3e6-1c3ca52a994e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "58e9df44-df75-499b-9514-ffea117022c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "67b5e665-3afe-4283-a3ce-0dd1d603172f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 187
        },
        {
            "id": "825e48c0-1429-4365-8093-17b938c6a1d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 192
        },
        {
            "id": "5534ca61-e2a1-464d-a323-e47aa25d6fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 193
        },
        {
            "id": "35a321aa-b896-45b1-b484-fefcccc74c56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 194
        },
        {
            "id": "6226482f-418a-4e2d-a5d3-de4086e21ca6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 195
        },
        {
            "id": "bc67a366-6092-4125-af6c-0df3a5fb25aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 196
        },
        {
            "id": "60d5f89a-4142-4145-b702-68e39ba467d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 197
        },
        {
            "id": "4162272e-f00f-4b14-a68b-d9ad454e4ecb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 198
        },
        {
            "id": "99937776-621b-4bee-875f-7c32db6c13be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 199
        },
        {
            "id": "2adbe90f-b32f-4846-8488-e3796c846eae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 210
        },
        {
            "id": "4eb5b14d-ad04-42b4-b7e1-4829acd35a1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 211
        },
        {
            "id": "34a6a389-8460-4a80-b866-d968b707d97d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 212
        },
        {
            "id": "21e57d8e-bc74-483e-be94-d3a7afff4eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 213
        },
        {
            "id": "b0f9dd6c-766d-4c5d-8bf2-62f6d41653c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 214
        },
        {
            "id": "f99dce4e-4690-4d58-b8ac-3e08de1e82fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 216
        },
        {
            "id": "81f95830-2748-494e-8f03-1fcfa8c7e10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 224
        },
        {
            "id": "f5bb35e5-bffb-453f-a4b9-c4bbc522f1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 225
        },
        {
            "id": "df3cc9d0-8bdd-4a2e-841b-11781e38b962",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 226
        },
        {
            "id": "85dcd7fe-525f-40fb-a2bd-a0c8b0cd781e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 227
        },
        {
            "id": "709a3280-4cf5-44a6-ba0e-ac8461d07fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 228
        },
        {
            "id": "127b46e4-2796-44d5-ac79-9876a53ed1d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 229
        },
        {
            "id": "33b8cbd8-39d6-485a-aa73-c9f264fec1f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 230
        },
        {
            "id": "317438a6-02cb-4b13-a8ee-d624e01e65ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 231
        },
        {
            "id": "747ea0b1-a279-4c46-bb87-d2fd8da7a4e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 232
        },
        {
            "id": "3036e456-4ea2-4810-a259-236ea8dd3d18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 233
        },
        {
            "id": "4512d5a0-b7de-4848-8ec2-ffc3a39bdef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 234
        },
        {
            "id": "08b16640-f877-4dd8-bfbd-40294cae5cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 235
        },
        {
            "id": "60f6d433-50b2-4cc3-b310-8a8b96c07bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "84469d25-c7c2-4fd4-9a39-9ed2b257358f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 239
        },
        {
            "id": "ffca6bbc-5c34-4ecb-ab1f-47b1c0a4cc78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 241
        },
        {
            "id": "28a54dee-0b41-4574-9d53-cbf6da78a0e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 242
        },
        {
            "id": "47477b96-da3f-4e88-9d3b-eedea1485bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 243
        },
        {
            "id": "2c2a7c2f-69be-42ee-a89d-d950856a075c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 244
        },
        {
            "id": "3d9aa405-f9b3-4811-9a15-45d549505936",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 245
        },
        {
            "id": "497951d1-4fa1-43bf-9a33-1d6ae44cec7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 246
        },
        {
            "id": "54b90dc5-2534-480a-a8fd-0aae4fc93e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 249
        },
        {
            "id": "b4f01ebd-20e9-43a7-a0b1-270596d93b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 250
        },
        {
            "id": "11af1520-ffdb-4e5e-8ca8-3395387c1f21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 251
        },
        {
            "id": "a1651b85-feb8-4b39-9ce9-361a5be7bbb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 252
        },
        {
            "id": "d6b1cfc4-cb5a-4850-9f62-764c7922021c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "00e854ab-9a56-429c-b7e2-3fb6b9dc812e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "700c3b1f-96b1-4640-8f5a-e811c4a6ec95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 338
        },
        {
            "id": "cd6a6cdb-5bfa-4e71-81b0-a3a230323832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 339
        },
        {
            "id": "7f11ef4f-a8c0-4382-80ce-6a4b1d33f17f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 353
        },
        {
            "id": "892b51a3-31e3-4909-8ba0-31aa419505eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 382
        },
        {
            "id": "607b97cd-8e74-4a5e-9d2b-9a382905d621",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8217
        },
        {
            "id": "a4f674a7-4a21-45d5-ac24-0b8d4a75f7f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8218
        },
        {
            "id": "0cb7354b-446e-42f6-97bf-590b9e3f3e24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8221
        },
        {
            "id": "8eca4582-dba1-4077-ad66-b4b9758fc4db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8222
        },
        {
            "id": "5e8cf64c-d276-4c2e-ba0d-73cb15544557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 8230
        },
        {
            "id": "fe97cb69-eb5d-4852-814f-0d01c9a410e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8249
        },
        {
            "id": "7cbc87d4-7b2d-4611-8afd-5a0bc6ea6cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8250
        },
        {
            "id": "2a85ca6a-e317-4fa1-8a0f-6e368218f98f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 8482
        },
        {
            "id": "737526fd-eaa7-448c-873d-bde208f51b68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "6dd2c793-cffe-424e-8e67-629abd443502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "a2e18573-23d3-4fce-bbc2-5930461686ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 85,
            "second": 198
        },
        {
            "id": "b6571b47-be87-4b96-bea3-3c9479d2994b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8218
        },
        {
            "id": "70cca067-59f3-4d7a-b83e-196b1a0ca027",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 8222
        },
        {
            "id": "8ac8152f-9e72-449b-8ca9-278bf7ca8de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "b3e276e0-5e02-4ca5-bf00-3b260251b4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "d377a5b3-75a0-4c10-835c-b31b9b01bc5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "95b1e8f5-8162-4d4b-9fb2-dccac73c5d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "1166f33b-76d7-43b7-8a45-10a52f97e8e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "682b782b-0de0-4bca-bade-7a9a1a29d202",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 67
        },
        {
            "id": "9f49dc55-0d52-4d09-bf51-0d9735be42a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 71
        },
        {
            "id": "7548d1e8-c9f1-485b-9468-99b98bced1be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 74
        },
        {
            "id": "74931047-bd8b-4396-a845-0b9dca2d2093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 84
        },
        {
            "id": "1ad4712c-f840-412d-8890-43fb5e3961a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "ccd133bd-0075-44cd-971f-992061699ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 99
        },
        {
            "id": "f38107ec-379d-4568-9138-bbd2cc31e70b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 100
        },
        {
            "id": "9c0f397d-c858-48fc-83fd-d2e8dc0f0308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "2b418d2b-e7f3-48ab-a263-89fa0544d619",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 103
        },
        {
            "id": "aa40ac05-3f5e-44aa-9ee9-f1c67b125090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 109
        },
        {
            "id": "15d418df-4cd2-4d64-abf0-e562a4984019",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 110
        },
        {
            "id": "f4a1e1da-7d53-469f-9ab4-0c3b6a7dcc67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "b2f07307-8be7-4d6a-9e66-a6dc86a58436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 112
        },
        {
            "id": "84f754af-c62d-4ec1-a1d8-31ed4f5837fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 113
        },
        {
            "id": "d0ab8686-3003-4568-b6ce-4b67ed902ec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "c8e14089-93fa-485e-aa62-2a1e5762ad71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "cd186d7b-fda0-4866-a65b-7e475c877de9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "8423bca1-ab2d-4384-a783-706139fc36d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 171
        },
        {
            "id": "d972a27c-6aac-4f66-b514-aa53bf0b1a3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 191
        },
        {
            "id": "a1ca2c1f-709a-472c-88cf-cc29f26d132a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "66fc1c12-1004-44c7-9ccb-e6899f316a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "222bc4f7-685a-44f4-96e2-6c227da8eea0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "d49faca3-d311-4fd4-885a-6f26ca9c2667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "14d22e5e-a1f9-4e75-9edd-d4bdb26d22ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "85a64259-96c2-4b7e-bc3a-6ea2c4dc6b6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "05efb59c-d392-45d2-8f1c-f0f54fb0d5d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 198
        },
        {
            "id": "f23bab7a-2fa6-422c-9479-5ecc2ecdb57e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 199
        },
        {
            "id": "2f18dc65-23ea-4c7d-be56-4d4f372ee769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 224
        },
        {
            "id": "a865ca3c-f8de-4906-a491-c3b1a0f420e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "7c470d3b-3abe-4246-9282-eed986b937a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 226
        },
        {
            "id": "023f8ba4-8170-476e-85ef-b4fda7259a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 227
        },
        {
            "id": "e1230515-ce14-48e1-9f22-4d015c3f190c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 228
        },
        {
            "id": "3adf0ce2-19b9-4b5a-b49d-9709cd0352b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 229
        },
        {
            "id": "598410c5-d294-4cf0-ba45-a49512684311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 230
        },
        {
            "id": "1393e2e3-2574-4562-8712-3c1f5d9d1620",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 231
        },
        {
            "id": "62953e76-cc94-480a-ab82-3e340b549b3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 232
        },
        {
            "id": "e445bb07-7c33-4ed2-b868-c36cd1ca8bcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 233
        },
        {
            "id": "942b996a-3b2e-49d8-88aa-8bec9f4175d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 234
        },
        {
            "id": "82c3de36-8cc4-47ff-8a68-1267b113b7a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 235
        },
        {
            "id": "3f0b3a2f-f130-4942-8178-bc9610f27fe7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 239
        },
        {
            "id": "4dfecc28-d9a9-41d6-9d1a-0a4ea2e5c88d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 241
        },
        {
            "id": "f3c82a07-6b37-4c6c-90fe-11c3fbc2ac63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 242
        },
        {
            "id": "e784b6d8-4cc6-4ed6-9971-e709872a287a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "1be44430-c5c5-43e8-82fe-0ebee90104de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 244
        },
        {
            "id": "a74be43c-4c43-4b4b-b736-232c36d4e279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 245
        },
        {
            "id": "7fa5f94f-b195-4492-9d44-863df0f14c05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 246
        },
        {
            "id": "f6c429f9-3455-42de-a91d-713e89573832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "38b912ef-c5ba-4259-88bc-2d8264a0a0ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "cebf6be0-55e1-44d1-8ae5-b615b288675b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 251
        },
        {
            "id": "26da5995-bc75-43ab-806f-287d5478ce4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "9316effd-e49e-4cfd-8d8d-52a5685213ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 339
        },
        {
            "id": "b6d5b1b9-0af7-46f3-bd55-69d5c28eb379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "b7cbd320-3546-4c35-9575-fa1b592351ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8218
        },
        {
            "id": "e1b2dac4-5fa2-4f5a-aa0b-2af193a0f952",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8222
        },
        {
            "id": "4a8a00ff-d77a-4978-bc3c-58e5699a8f4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8230
        },
        {
            "id": "5e59eb9d-c505-4e1b-8357-028c620f2f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8249
        },
        {
            "id": "18f02676-8ad0-4ac9-98aa-6d19330eb5e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 8482
        },
        {
            "id": "a6a3f0fa-d2ec-497b-a9be-86f6957bd296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "8a4e4403-4da6-4677-a5b2-e4cf900659c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "d362cd5c-1ed6-47a2-a625-7b3aca4dadf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "90774906-29ec-43fe-9438-7060bf50912c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "76a4a322-f7d3-4bf1-89cd-304756abf8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "c8d88d50-147f-4e31-ace7-5f82d1c9542f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "1841ff0e-3c0d-4cc6-9661-d5be23907388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "3e881997-d108-4b50-b196-da04190adef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "903fed64-20a8-4317-b91f-b264fcab7a74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "46a4ce73-ede6-4d5b-8215-93673e922137",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "4371ffc9-e8f4-4fb4-9766-957f7de60dd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 171
        },
        {
            "id": "d912e0ab-ecfa-498c-a3b8-62fe2935c720",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 187
        },
        {
            "id": "a370f6fa-f3cb-45c4-b47c-ba5a522c7d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 191
        },
        {
            "id": "a9c1fb67-64da-4a93-9833-da2df80b354a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "f6d1c7f2-29cd-4729-9a5c-9cef560f7a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "5286898a-7db9-4d69-abee-f7e9f3b37296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "c7d42417-5aba-49a9-b197-65026661a192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "3761a3a0-72d5-4399-b08e-ef26f0329cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "e925ede9-7213-4273-a653-d8ce5a6bd2e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "528275bf-9319-4d1d-a9d3-00b6ea76a13a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 198
        },
        {
            "id": "85a36bf8-519f-4226-9e07-28ddfb019cd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "e260ceba-9bb9-4690-a142-87fce4bc06ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "ffdd2810-189e-4b78-997d-8cd9b1ee236c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "e510b3e3-0b47-476b-8209-05f0d8f6bd1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "38e3ec41-a30f-4ff2-9695-c2deafcf78b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "2d7c4dba-0e48-4e6d-a5f5-9627ee4f1af3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "1a1908de-6f57-4876-92a3-ecc9c0cee583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 230
        },
        {
            "id": "07a470c1-97fa-466c-8f1f-ded68f862e09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "fa9ecd2a-c4a7-4dcd-8410-0ec07fe93184",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "d096487c-006e-4332-87e2-c9518c8b7645",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "18253492-b663-4702-b871-4d45936a7d3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "5768c8a6-d903-4150-94f0-316aef51af9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "a49f00db-bcdc-4214-b9f6-1c7f3a26cc33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 238
        },
        {
            "id": "b8288310-f0f0-426a-b363-1635370f2ace",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 239
        },
        {
            "id": "53ae3445-6015-495e-bb50-935470f0f8a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "930a46b0-125d-4935-884c-f89af2fa0bd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "a1a3cfb6-4273-4be9-b585-2559c1531cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "3ba57582-5a61-4c67-9d62-caa2a1f1fb3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "11d1ccbd-d5ad-48cc-9c43-cc4d7369b874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "5ee89f31-fce9-4e64-bdee-0cf777e5837e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "10501651-c1dd-4113-a914-0697c89bf803",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8218
        },
        {
            "id": "ca77c03d-9839-40a5-99b2-5ebf31781fb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8222
        },
        {
            "id": "d4fa1444-6aac-4dd7-8cc7-98953dba762e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8230
        },
        {
            "id": "e6ee007a-5637-486c-8a5f-e2a7d6143ac2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8249
        },
        {
            "id": "8a4d6ab0-b03e-4407-a6f9-b69bb3bf59c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 8250
        },
        {
            "id": "d542e073-9bab-4536-9f5f-4eeebf81aa7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "534b8dcd-271d-447b-8495-39438756be3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "f4110710-be8d-4364-a17a-19e7cafd3d7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 59
        },
        {
            "id": "32bd4767-49f5-48de-a6a6-2e56585a4ec4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 74
        },
        {
            "id": "5b1e807c-b443-4a88-8612-6ac10177be9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 84
        },
        {
            "id": "12a44d1d-60f4-4253-b712-37e77fe9cb0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 239
        },
        {
            "id": "7fd15238-eb20-4946-8f1d-21d82a8c07e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8218
        },
        {
            "id": "1869068f-7000-40a7-8273-d9989e6f2174",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8222
        },
        {
            "id": "cc772f53-d99a-45e6-a8f7-cfdc13785a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 8230
        },
        {
            "id": "f013c1a5-a0b3-4abd-996d-1713177dcc12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "66574227-f48b-4d04-8103-133b35b587ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "5361dfff-8c82-4b24-83e4-9bbdd413e115",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "828d5c17-752c-4071-85d5-6c3882c8a52b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 67
        },
        {
            "id": "f72eb450-a5ad-4104-9f1c-190fa95e880f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 71
        },
        {
            "id": "170258ad-4bc1-42c4-853b-0661ce713461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 74
        },
        {
            "id": "d923520c-924f-42b4-b4ad-9658562c7252",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 79
        },
        {
            "id": "138d566f-3c67-402d-b6e0-be8915213d5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 81
        },
        {
            "id": "2cd9b819-2bd4-49f2-a8dc-0f72af1ebd2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 84
        },
        {
            "id": "c7ae558d-4a6a-44f7-92c4-fe87dff42810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 97
        },
        {
            "id": "b2b5f908-e370-4ebe-9f0d-402dc696eb05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 99
        },
        {
            "id": "ca227347-1b45-4076-8ef7-5bc04ee3820b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 100
        },
        {
            "id": "605e5f56-c921-4496-9a83-d7bb0a5e35d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 101
        },
        {
            "id": "032a5442-53fb-4aaa-b157-450df5f78802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 103
        },
        {
            "id": "991e8b58-8853-40eb-9b66-cc63f9edcc1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 109
        },
        {
            "id": "e3bf2812-c1aa-47b8-a0f5-66ad92ca59b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 110
        },
        {
            "id": "7e028933-badd-4bc4-a554-9c0a4a305be8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "b26723bc-9e19-4192-ac61-599d9260506f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "24b36bf1-b4ba-4d67-9790-f9630365829c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "99a478f8-2dcb-476d-956c-8f955544750f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "e4ab24ff-256a-4646-8eca-52e283b0cf4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 115
        },
        {
            "id": "20ae7304-77ea-4712-8146-5241e3ed94b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "65cf65eb-2f8c-4a64-a6b5-d711de6ec459",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 171
        },
        {
            "id": "d95568c1-693b-4547-9d59-d5166aa5a529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 191
        },
        {
            "id": "715c9213-359a-4663-8b22-3ee842a80906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 192
        },
        {
            "id": "d773f7ff-4ab0-412b-9122-c6783cad7025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 193
        },
        {
            "id": "a4beb2fd-84cf-476e-913d-512e6ea07946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 194
        },
        {
            "id": "69461dba-2649-41bb-bb4f-4f62a9fdb973",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 195
        },
        {
            "id": "a6e2180c-282b-496c-8752-0e03a44186d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 196
        },
        {
            "id": "d52f7b3b-66f5-4010-b9d5-0d965758f6ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 197
        },
        {
            "id": "e16232dd-73fb-45d4-a036-c16195f963dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 198
        },
        {
            "id": "5ccd5fcd-900a-46cc-9d8b-1797c54f6c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 199
        },
        {
            "id": "56c59dcb-cd2e-4d40-8ed5-638a8380e87c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 210
        },
        {
            "id": "c1124327-e9da-4ae4-8902-6d5dab16e430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 211
        },
        {
            "id": "6b05675e-e500-421a-9b33-463a839b49ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 212
        },
        {
            "id": "08a883fd-b297-4c06-bd61-7d088402c8bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 213
        },
        {
            "id": "d20b77a8-d7a3-4ebb-ba71-938af99f11b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 214
        },
        {
            "id": "51560e73-1a15-4c46-97c4-96bc5b55ed21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 216
        },
        {
            "id": "a59bf031-2768-4c7d-a28c-172d2b666d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 224
        },
        {
            "id": "1a037a3e-32c2-452a-9036-db210eafd22c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 225
        },
        {
            "id": "98031907-4e37-4780-a89e-89a058625c35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 226
        },
        {
            "id": "7cba91ed-2993-4f5e-8118-8489b158ff63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 227
        },
        {
            "id": "3bbf4853-1839-4d8b-bea0-15132e615b57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 228
        },
        {
            "id": "8f82340a-aa2d-4879-ba1e-c2d879574b40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 229
        },
        {
            "id": "91cd7b4d-43d0-4f7c-93f0-8bcd20047762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 230
        },
        {
            "id": "da22dcf9-7d1e-4e69-9ac7-1e487f7d5517",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 231
        },
        {
            "id": "1352393b-32d0-4635-abc9-9a1df26fdd66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 232
        },
        {
            "id": "e05fe678-095b-498c-9b9a-ec423ac5153c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 233
        },
        {
            "id": "bbf79bd3-2efe-4852-af49-fadf997a61c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 234
        },
        {
            "id": "e7147682-a8d1-4dcf-9c89-4d79dc040b4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 235
        },
        {
            "id": "d3d424f7-ad6c-41ed-8909-e533d0d56d33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 237
        },
        {
            "id": "af6ea572-cba2-46a6-8fda-327ca5e1e7f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 239
        },
        {
            "id": "e9e26080-a981-4dfb-b209-e3fbfdcf1f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 241
        },
        {
            "id": "43338f0c-41f4-4ae1-aaf9-054bc6fced4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 242
        },
        {
            "id": "b2104496-1244-417b-b6f9-713d9a588aae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 243
        },
        {
            "id": "7475a2fb-82b8-4034-a7bb-15e50085e5e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 244
        },
        {
            "id": "92bcc9c3-ef65-4bd9-998c-92c4aa6e3581",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 245
        },
        {
            "id": "3994fa50-07c4-4054-8596-30f42f8f6c59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 246
        },
        {
            "id": "5eb1eac4-c14a-4280-8137-1db2e1e66535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 249
        },
        {
            "id": "1cba3ba0-9576-468d-bbfb-73aa7b60e5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 250
        },
        {
            "id": "126e2a4c-8a11-45e7-97c9-22ca73a94533",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 251
        },
        {
            "id": "c9e404e7-f08e-4938-aee9-93ac735d60f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 252
        },
        {
            "id": "acade97b-fb3b-4291-b029-5362a3e69207",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 338
        },
        {
            "id": "89de56a2-9ce0-4e4b-a156-a52b7e7bd664",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 339
        },
        {
            "id": "c4efb2e1-f146-476d-bc4c-3cc9914038fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 353
        },
        {
            "id": "ded76170-5f6b-4b08-97a3-290a1a9518b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8218
        },
        {
            "id": "0229584d-7833-4270-97b7-129a5cf6aa49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 8222
        },
        {
            "id": "d9c193fa-eeed-4532-a120-9884b4383908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8230
        },
        {
            "id": "911b04fa-4b19-4ed3-b250-08b428352e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8249
        },
        {
            "id": "241548b7-ccb6-45e3-98a9-79043c4a9d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 74
        },
        {
            "id": "b2a1c2c3-3bb8-4f22-8ac4-7ad125cd09ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 84
        },
        {
            "id": "2746da75-c461-44cb-8a84-5fc26fbca32e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 121
        },
        {
            "id": "0a22d831-9107-418d-aec7-b0bb59d3f557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 187
        },
        {
            "id": "d11175c9-0da1-4a52-b2d4-6303391e4769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 239
        },
        {
            "id": "13c605db-d528-4284-8a28-d189f58d9c11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 253
        },
        {
            "id": "bf1180f4-f720-431b-8de7-cb8c74f661c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 90,
            "second": 255
        },
        {
            "id": "4928787c-01e9-4679-88ca-f68f6bc8084b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 90,
            "second": 8250
        },
        {
            "id": "6fff8738-24dc-4175-a27e-e92eb19f6512",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 91,
            "second": 106
        },
        {
            "id": "49c37460-3b21-4a3d-9d91-e29e12b59ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 120
        },
        {
            "id": "4ef5ce7b-00be-4447-9f87-a278bb023a2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 74
        },
        {
            "id": "fe432eda-8647-4f82-bc16-5d79686942b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "8a98b497-6751-4bd7-b362-9adff542c4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 89
        },
        {
            "id": "9633424f-942a-4320-b2f7-ec87d967836f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 221
        },
        {
            "id": "5ac72474-dea6-4f16-b063-653b912feab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 101,
            "second": 34
        },
        {
            "id": "3ad42ebe-fd5d-4446-867e-81add9589461",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 101,
            "second": 39
        },
        {
            "id": "95577d3e-9579-45e3-8967-acc3cf3710a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 41
        },
        {
            "id": "8e862e7a-23cf-4c17-a3fd-ecf764ad432d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 42
        },
        {
            "id": "0408d2cd-d285-4d3f-844e-2ea2ebf3182b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 44
        },
        {
            "id": "66d8bfe3-976d-4489-bba8-768913f9dae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 45
        },
        {
            "id": "df05205c-5faa-4e11-ae78-0a25cd1826f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 46
        },
        {
            "id": "d8e72c27-4701-417d-b457-4e23186f3369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 58
        },
        {
            "id": "7d1fca29-5069-4dac-a069-da8066e08e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 59
        },
        {
            "id": "3435b156-2ac7-4932-a781-128f579e56b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 63
        },
        {
            "id": "06f92d66-73c3-40a7-8b50-e954a230e691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 93
        },
        {
            "id": "52584484-7342-40f1-8e53-26a201eeb9ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 116
        },
        {
            "id": "701b9496-7325-4a7e-9a0c-def230ad1dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 118
        },
        {
            "id": "5ab46c28-1441-43db-8d61-32ae430f83e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 119
        },
        {
            "id": "c79ed989-f841-4afb-b5f7-db86ae17600a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 121
        },
        {
            "id": "b9754b5a-f10e-4cb8-95f2-0e4efc07d058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 125
        },
        {
            "id": "a70ce88f-76cf-4420-84d4-3e3eab8dcd7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 187
        },
        {
            "id": "6adf46f4-09b7-4476-bf20-594c7eb7e794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 236
        },
        {
            "id": "1cca5b03-bdfe-4b49-b88e-407eb87314ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 238
        },
        {
            "id": "b193dd5d-b449-4c3b-a6df-f352c1dc32cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 239
        },
        {
            "id": "c7b03ce5-33ad-4338-a858-719777adc0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8216
        },
        {
            "id": "6778bcae-b6d3-4775-88bc-e6ac5d61705a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "52fd4b89-ca5e-4325-9f78-e79d3c835e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8218
        },
        {
            "id": "a731969a-3512-4cfb-b839-b649f66fac94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8220
        },
        {
            "id": "29efa277-72c7-408e-940f-0fba9bac7de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8221
        },
        {
            "id": "bf1c85e0-47e5-4c56-a45f-4f9bdefa89fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8222
        },
        {
            "id": "4743fbad-e147-4928-8e46-c3a0f7bf3e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 8230
        },
        {
            "id": "1597265a-1cdd-409f-8913-c4b48fb34b16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8250
        },
        {
            "id": "bff0355f-3331-4e3c-b96d-525a4edbf355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 8482
        },
        {
            "id": "a3c9c5f5-898c-4ced-9290-21afba1b3cf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 44
        },
        {
            "id": "75c1edcb-3519-4c08-a7c2-04acaf8a052e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 107,
            "second": 45
        },
        {
            "id": "fe0832cc-2648-43aa-96ab-ac623f77c4c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 46
        },
        {
            "id": "577339b3-eb36-4bf6-b96d-e107e40b84e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 58
        },
        {
            "id": "a2ed88c5-9138-4c0d-a3d4-3cea67e740f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 59
        },
        {
            "id": "ad8d0b87-c9b8-49d3-9697-8dc20ce059f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 107,
            "second": 8230
        },
        {
            "id": "b0035ee2-0be3-4db4-9c9b-39d75cd22446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 34
        },
        {
            "id": "20b9108f-ae63-4a25-a691-05150fe2e541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 110,
            "second": 39
        },
        {
            "id": "90c1eacd-c375-4380-b12d-0751f827c53c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 34
        },
        {
            "id": "c73a3327-25d9-44da-af35-baf8b2b5f553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 39
        },
        {
            "id": "fc1346f6-a3bc-46a1-b864-a6ffc3450958",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 120
        },
        {
            "id": "c89cc6bc-093b-4a87-8b3f-e163b9312cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8216
        },
        {
            "id": "c605f6e8-78bb-4a93-9fbe-2c2659e6d1d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 8217
        },
        {
            "id": "28108e9a-7b7c-4a9d-be8e-1a7bd9e03c1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8220
        },
        {
            "id": "802dce61-f817-423f-bde3-501fd8a6f195",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 111,
            "second": 8221
        },
        {
            "id": "72c7bd73-efeb-48c4-a2b4-a6c4e6284009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 120
        },
        {
            "id": "4a53d215-5fea-4f90-91eb-d47cdd5256d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8216
        },
        {
            "id": "8c39468c-0df0-4485-9981-56805d58b97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8217
        },
        {
            "id": "c60e152d-1157-4891-9ee2-7aa8df7d7a19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8220
        },
        {
            "id": "dde23031-71a2-4ef9-a353-556936b9d203",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 112,
            "second": 8221
        },
        {
            "id": "9283b5a5-1b33-49e9-96a0-2a88893d3a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 113,
            "second": 106
        },
        {
            "id": "8278f9bf-39a6-45c0-9056-1691b3c7878c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "590c508f-9344-4857-b6e3-b796013b560d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 45
        },
        {
            "id": "bcb73ead-7175-4b23-a5b8-93d96d001abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "54137b2c-3411-4039-83e6-4938b944ca17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 58
        },
        {
            "id": "4c784c3d-cd7c-4e53-a315-205fe1fcc63c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 59
        },
        {
            "id": "6b867806-221e-4a24-afd8-a3a40995dc98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 102
        },
        {
            "id": "2d30b17f-ec6a-4606-beca-fb2876b21d47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 116
        },
        {
            "id": "44d4dcb8-cefa-491a-a205-d2d513cabe28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 118
        },
        {
            "id": "97707a68-6b04-47e6-a50a-11654da3f0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 119
        },
        {
            "id": "8d411657-cc75-4127-9a37-f5b84fba14e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 120
        },
        {
            "id": "3f980136-4f82-41f5-9fae-ec06d342c7c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 121
        },
        {
            "id": "ab8f37d7-28e7-4b32-b3c4-0cdacfccec82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 122
        },
        {
            "id": "d21efa70-b30d-43f2-98a8-633b6c79446e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 253
        },
        {
            "id": "228c71dc-c8d2-4279-9666-ee3d3042ecce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8216
        },
        {
            "id": "3eaf53e2-4e7a-43f9-ba68-0005c53fc61a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "bedb225a-2890-42b3-a0be-295cc2c59ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8218
        },
        {
            "id": "456320b1-7ca2-4872-b59d-89739deafd21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8220
        },
        {
            "id": "a7079da4-70fe-49ff-845d-34f2e8f13886",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8221
        },
        {
            "id": "5157c197-82ca-4479-a4d7-b832f1f6a39b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8222
        },
        {
            "id": "4ad2544c-2cd4-4ed0-92fe-55b91ef5fa33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8230
        },
        {
            "id": "251263e1-b9ae-4d0a-a94c-58ae5142eb02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 45
        },
        {
            "id": "ddc8a214-dd33-4d25-b82e-0147192aa968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 63
        },
        {
            "id": "dd3517fc-52d3-4dbb-8934-f2a34bc54c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 187
        },
        {
            "id": "b492e271-d81a-4f05-8bb0-8d8a407f98c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 116,
            "second": 8250
        },
        {
            "id": "c1ba0286-11a6-43a7-a513-6c66ee5bbd2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 34
        },
        {
            "id": "6cb4f853-d66e-40c8-8e58-a26747eb6440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 117,
            "second": 39
        },
        {
            "id": "293b8baa-949d-406a-be0b-433e06fdf127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "0d3cf9af-0bc5-4adc-94d1-5c9d13f6613e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "1c364b1b-fa8a-4071-9ed1-fcf982a6e7a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8218
        },
        {
            "id": "13faadd8-58bd-4b8a-9706-d842136e717f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8222
        },
        {
            "id": "afd5dcf1-160d-4239-98bf-a4919146a6a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 8230
        },
        {
            "id": "bc921547-e6d5-4b3d-821e-32113fd8c5f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "3c919e11-91d0-401a-801f-def94fd68c6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "56f15680-c3f3-40d3-aa3d-6c5bd42155e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8218
        },
        {
            "id": "632493bf-03a5-43c1-a4ce-82512a9a2b45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8222
        },
        {
            "id": "a31f35d6-8fef-4bd5-a63b-bf971f79290a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8230
        },
        {
            "id": "8459fa08-e89d-4190-9115-20cce4fb3651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 99
        },
        {
            "id": "8bd9ecdc-e1de-439d-9a80-b2b88f6505b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 100
        },
        {
            "id": "86530519-c852-431a-83dd-6849d0c49564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 101
        },
        {
            "id": "11c4f3a0-555d-4d54-8c97-830ce469d651",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 103
        },
        {
            "id": "0dc27bb4-b313-447d-878b-58dc2386979b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 111
        },
        {
            "id": "c2cb9580-8b28-48f0-8602-46b1c6d6b218",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 113
        },
        {
            "id": "9176075c-2513-4286-bb4b-94e5538ec5f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 232
        },
        {
            "id": "8beff3d9-7a88-44cb-8e4d-fe25b2e43010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 233
        },
        {
            "id": "ab1ba864-599e-4023-8f24-9ca901518f40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 234
        },
        {
            "id": "18aebdb7-9a4b-4855-94fa-58be6f243932",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 235
        },
        {
            "id": "f02dd776-3b14-4f71-87ea-a60d60d453f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 242
        },
        {
            "id": "eccd0a35-bc8f-4939-9f47-fc0fb883703b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 243
        },
        {
            "id": "2f81e240-2a2e-4447-affe-f58e44731508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 244
        },
        {
            "id": "bc7fbd5b-32b0-483e-aaae-441779a6f2fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 245
        },
        {
            "id": "d93d5638-bfa0-41ad-acfd-309122650046",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 246
        },
        {
            "id": "732c863c-b7e0-433b-8285-99704f9f027f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 339
        },
        {
            "id": "40fe8277-f370-40ae-a5f3-a690cba8409d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 121,
            "second": 39
        },
        {
            "id": "0ebdfea9-ccb8-4679-b669-ff1c019b4a72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "1a28b9b8-c927-4cee-b1dd-ad785d6de93a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "58291879-dfc8-4beb-ad41-dd1f13e6b697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8218
        },
        {
            "id": "eeaa6ecc-0a60-485d-b41d-8c45b3dd6509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 8222
        },
        {
            "id": "a310998e-6b11-4a39-b576-94db2b743dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8230
        },
        {
            "id": "3b93e649-b9ef-49b6-be7d-e10bc0d35d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 123,
            "second": 106
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 23,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}