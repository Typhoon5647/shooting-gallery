{
    "id": "1914408e-2d35-4bce-ad60-bcaa21fff27f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 0,
    "bbox_right": 141,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa04565a-108a-4ccc-b0cf-f0832fe14394",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1914408e-2d35-4bce-ad60-bcaa21fff27f",
            "compositeImage": {
                "id": "419c85f4-aeb6-42eb-a096-7ab0c1bb017a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa04565a-108a-4ccc-b0cf-f0832fe14394",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7393c27c-51ba-4443-b700-d671cda57638",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa04565a-108a-4ccc-b0cf-f0832fe14394",
                    "LayerId": "a154eb0b-8ed5-4865-a37b-bcd3a6900792"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 142,
    "layers": [
        {
            "id": "a154eb0b-8ed5-4865-a37b-bcd3a6900792",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1914408e-2d35-4bce-ad60-bcaa21fff27f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 142,
    "xorig": 71,
    "yorig": 71
}