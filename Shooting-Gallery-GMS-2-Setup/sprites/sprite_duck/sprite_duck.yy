{
    "id": "f6cc6b0a-b98f-4145-ad5d-4b07d5bf5886",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_duck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 108,
    "bbox_left": 0,
    "bbox_right": 113,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11be3a94-6051-4c90-a17c-ab63c73b07b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f6cc6b0a-b98f-4145-ad5d-4b07d5bf5886",
            "compositeImage": {
                "id": "7bcdef59-925f-4f09-895d-eeca2ab360b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11be3a94-6051-4c90-a17c-ab63c73b07b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e122b575-15b0-491d-a33a-077beda31889",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11be3a94-6051-4c90-a17c-ab63c73b07b3",
                    "LayerId": "41245976-839f-421a-b080-ecb71e7e0b17"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "41245976-839f-421a-b080-ecb71e7e0b17",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f6cc6b0a-b98f-4145-ad5d-4b07d5bf5886",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 108
}