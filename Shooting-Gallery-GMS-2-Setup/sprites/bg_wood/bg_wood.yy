{
    "id": "4c5112e0-2060-408e-a00d-68c9a47d77ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_wood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 255,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d63e217f-61d9-4153-b860-9709155a2a8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4c5112e0-2060-408e-a00d-68c9a47d77ec",
            "compositeImage": {
                "id": "469a2f5a-7eb0-404f-9074-25713b130676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d63e217f-61d9-4153-b860-9709155a2a8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "64afe51d-5ca0-48a2-b43d-7c867489190c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d63e217f-61d9-4153-b860-9709155a2a8e",
                    "LayerId": "b3c00c69-6780-4d39-a75e-f444bf4fab00"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "b3c00c69-6780-4d39-a75e-f444bf4fab00",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4c5112e0-2060-408e-a00d-68c9a47d77ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}