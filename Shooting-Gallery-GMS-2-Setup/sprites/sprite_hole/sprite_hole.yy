{
    "id": "c688a3da-796b-4b78-b8c5-e47601a17548",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_hole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "925d44a3-5858-4db5-ae79-368410f87677",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c688a3da-796b-4b78-b8c5-e47601a17548",
            "compositeImage": {
                "id": "0a3c65b0-2d7b-4c42-9b51-33797b49bafa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "925d44a3-5858-4db5-ae79-368410f87677",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a550ac86-c4fb-44cd-8c96-51d239e21a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "925d44a3-5858-4db5-ae79-368410f87677",
                    "LayerId": "6187f43e-836e-44ca-9556-1f2fe09c268f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 30,
    "layers": [
        {
            "id": "6187f43e-836e-44ca-9556-1f2fe09c268f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c688a3da-796b-4b78-b8c5-e47601a17548",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 15
}