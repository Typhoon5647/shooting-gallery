{
    "id": "3541c235-f9fc-420e-82f3-c4d228de9721",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_target_bullets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 141,
    "bbox_left": 0,
    "bbox_right": 141,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ca4cd37-e89c-43df-b7a9-0ff415feb2b6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3541c235-f9fc-420e-82f3-c4d228de9721",
            "compositeImage": {
                "id": "34f3f16c-42c4-4345-b388-0b02088fa48c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ca4cd37-e89c-43df-b7a9-0ff415feb2b6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d7f64f48-0ba8-490e-a2d4-e9541ac75e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ca4cd37-e89c-43df-b7a9-0ff415feb2b6",
                    "LayerId": "bd3ae107-c793-4ff0-98c4-9a6a05a4251c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 142,
    "layers": [
        {
            "id": "bd3ae107-c793-4ff0-98c4-9a6a05a4251c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3541c235-f9fc-420e-82f3-c4d228de9721",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 142,
    "xorig": 71,
    "yorig": 71
}