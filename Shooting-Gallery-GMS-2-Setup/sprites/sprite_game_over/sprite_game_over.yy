{
    "id": "3b4c8159-e119-4512-b1a9-3b3a74dbf14f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_game_over",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 0,
    "bbox_right": 347,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d18542a2-bdd0-413d-8e85-507c7a8e0f37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b4c8159-e119-4512-b1a9-3b3a74dbf14f",
            "compositeImage": {
                "id": "712ba638-0b69-462d-9834-adfb7ac281ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d18542a2-bdd0-413d-8e85-507c7a8e0f37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c7d1736-40a1-4dd6-bc9a-da5a224452c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d18542a2-bdd0-413d-8e85-507c7a8e0f37",
                    "LayerId": "4ebfb400-7799-4e27-a6a8-826ac1052e57"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 72,
    "layers": [
        {
            "id": "4ebfb400-7799-4e27-a6a8-826ac1052e57",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b4c8159-e119-4512-b1a9-3b3a74dbf14f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 349,
    "xorig": 174,
    "yorig": 36
}