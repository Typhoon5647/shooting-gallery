{
    "id": "3c680a72-257b-430a-aa8d-6a5fbcfc13aa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_water",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 767,
    "bbox_left": 0,
    "bbox_right": 131,
    "bbox_top": 568,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34789973-1027-49fb-b208-7ba076cccd5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3c680a72-257b-430a-aa8d-6a5fbcfc13aa",
            "compositeImage": {
                "id": "941dee89-1aba-4faf-bf78-10a6a63f8802",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34789973-1027-49fb-b208-7ba076cccd5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab9e9807-fdc0-4236-8284-06e974340462",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34789973-1027-49fb-b208-7ba076cccd5a",
                    "LayerId": "cc6e491e-7871-40bc-8109-f2ec984c0657"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 768,
    "layers": [
        {
            "id": "cc6e491e-7871-40bc-8109-f2ec984c0657",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3c680a72-257b-430a-aa8d-6a5fbcfc13aa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 132,
    "xorig": 0,
    "yorig": 0
}