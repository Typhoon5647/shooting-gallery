{
    "id": "a91c24d4-8c58-4636-8c9d-c15f55c4760a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_duck_target",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 93,
    "bbox_left": 34,
    "bbox_right": 72,
    "bbox_top": 57,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "90e5bb81-a62e-4e70-8709-a0d67ae93442",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a91c24d4-8c58-4636-8c9d-c15f55c4760a",
            "compositeImage": {
                "id": "bf4cc698-3e0b-4c63-8b40-ea35b44fa163",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90e5bb81-a62e-4e70-8709-a0d67ae93442",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5ca34249-b54a-433a-ba0f-99d3359da2e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90e5bb81-a62e-4e70-8709-a0d67ae93442",
                    "LayerId": "31cbbb0e-71b9-4bf7-bcaa-c517c86a52fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 109,
    "layers": [
        {
            "id": "31cbbb0e-71b9-4bf7-bcaa-c517c86a52fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a91c24d4-8c58-4636-8c9d-c15f55c4760a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 108
}