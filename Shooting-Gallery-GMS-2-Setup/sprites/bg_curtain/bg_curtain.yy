{
    "id": "c519913c-a65a-481a-af01-3934980e28ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "bg_curtain",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 79,
    "bbox_left": 0,
    "bbox_right": 255,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b864750c-7ab2-445e-b7b5-0c98c0062d39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c519913c-a65a-481a-af01-3934980e28ad",
            "compositeImage": {
                "id": "75de774f-3ce3-4d54-9a30-cbfb2ab4c2e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b864750c-7ab2-445e-b7b5-0c98c0062d39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bd5023f-3363-4fb8-a5a8-2a1b2ae6ac5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b864750c-7ab2-445e-b7b5-0c98c0062d39",
                    "LayerId": "dd934d26-6c54-4d14-a43d-dd63c0545a5c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 80,
    "layers": [
        {
            "id": "dd934d26-6c54-4d14-a43d-dd63c0545a5c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c519913c-a65a-481a-af01-3934980e28ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}