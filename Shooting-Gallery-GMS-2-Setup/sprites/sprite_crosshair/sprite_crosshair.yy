{
    "id": "6b13dcd2-2334-4593-bac5-12446eff3b12",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_crosshair",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 49,
    "bbox_left": 0,
    "bbox_right": 49,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0f6b666f-132f-40e0-960f-930e3f614dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6b13dcd2-2334-4593-bac5-12446eff3b12",
            "compositeImage": {
                "id": "58c90442-160b-40dc-9794-b8c6f9125835",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0f6b666f-132f-40e0-960f-930e3f614dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4abe0dc-06bc-4826-9a97-9c0fc12cba80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0f6b666f-132f-40e0-960f-930e3f614dfc",
                    "LayerId": "1e267cc8-a6cb-4200-b18c-1d24efee20ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 50,
    "layers": [
        {
            "id": "1e267cc8-a6cb-4200-b18c-1d24efee20ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6b13dcd2-2334-4593-bac5-12446eff3b12",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 50,
    "xorig": 25,
    "yorig": 25
}